package edu.fau.android.tba.project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Toast;

public class Opening extends Activity{
	
	// Set the display time, in milliseconds (or extract it out as a configurable parameter)
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opening);
        
       
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                //Finish the opening activity so it can't be returned to.
                Opening.this.finish();
                // Create an Intent that will start the main activity.
                Intent mainIntent = new Intent(Opening.this, Main.class);
                Opening.this.startActivity(mainIntent);
            }
        }, SPLASH_DISPLAY_LENGTH);
	}
	
}
