package edu.fau.android.tba.project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TrackView extends Activity {
	private String project_name, track_name, track_title;
	private TextView title;
	private Button play, stop;
	private Player player;
	private Effects FX;
	private Boolean t1e, t2e, t3e, t4e;
	private Drawable btn;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trackview);
        Bundle extras = getIntent().getExtras();
        FX = new Effects();
            project_name = new String(extras.getString("name"));
            track_name = new String(extras.getString("track_selected"));
            track_title = new String(track_name);
            title = (TextView)findViewById(R.id.trackname);
        	player = new Player(project_name);
        	Load();
        	setTrackExists();
        	play = (Button)findViewById(R.id.playbtn);
        	stop = (Button)findViewById(R.id.stopbtn);
	}
	
	/*
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    Intent start_projectview = new Intent(TrackView.this, CEN4214_Project_TBAActivity.class);
	    start_projectview.putExtra("name", project_name);
	    start_projectview.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    startActivity(start_projectview);
	}*/
	
	public void onTrackViewBtnClick(View v) {
		switch(v.getId()) {
		case R.id.playbtn:
			if(!player.playing){
    			//start playing
        		player.Play();
        		btn = play.getBackground();
        		play.setBackgroundColor(Color.GREEN);
    		}
			break;
		case R.id.stopbtn:
			if(player.playing){
    			//stop playing
        		player.Stop();
        		play.setBackgroundDrawable(btn);
    		}
			break;
		case R.id.reversebtn:
			Reverse();
			break;
		case R.id.delaybtn:
			Delay();
			break;
		}
	}
	
	//Set current track.exist = true, and all others to false for modification of one audio file
	private void setTrackExists() {
		if(track_name.matches("track1")) {
        	track_title = "Edit Track: Track 1";
        	if(t1e){
        		player.mixer.track1.exists = true;
        		player.mixer.track2.exists = false;
        		player.mixer.track3.exists = false;
        		player.mixer.track4.exists = false;
        	}
        } else if(track_name.matches("track2")) {
        	track_title = "Edit Track: Track 2";
        	if(t2e){
        		player.mixer.track1.exists = false;
        		player.mixer.track2.exists = true;
        		player.mixer.track3.exists = false;
        		player.mixer.track4.exists = false;
        	}
        } else if(track_name.matches("track3")) {
        	track_title = "Edit Track: Track 3";
        	if(t3e){
        		player.mixer.track2.exists = false;
        		player.mixer.track3.exists = true;
        		player.mixer.track1.exists = false;
        		player.mixer.track4.exists = false;
        	}
        } else if(track_name.matches("track4")) {
        	track_title = "Edit Track: Track 4";
        	if(t4e){
        		player.mixer.track2.exists = false;
        		player.mixer.track3.exists = false;
        		player.mixer.track1.exists = false;
        		player.mixer.track4.exists = true;
        	}
        } else {
        	track_title = "Edit Track: Unknown Track";
        }
        title.setText(track_title);
	}
	
	//If the current track exists, then call the Reverse effect for that track
		private void Reverse() {
			if(player.mixer.track1.exists) {
				FX.Reverse(player.mixer.track1);
			} else if(player.mixer.track2.exists) {
				FX.Reverse(player.mixer.track2);
			} else if(player.mixer.track3.exists) {
				FX.Reverse(player.mixer.track3);
			} else if(player.mixer.track4.exists) {
				FX.Reverse(player.mixer.track4);
				}
		}
		//If the current track exists, then call the Delay effect for that track
		private void Delay() {
			if(player.mixer.track1.exists) {
				FX.Delay(player.mixer.track1);
			} else if(player.mixer.track2.exists) {
				FX.Delay(player.mixer.track2);
			} else if(player.mixer.track3.exists) {
				FX.Delay(player.mixer.track3);
			} else if(player.mixer.track4.exists) {
				FX.Delay(player.mixer.track4);
				}
		}
		private void Load() {
			FileInputStream fis;
			try {
				File main_directory = new File(Environment.getExternalStorageDirectory(), "Multi Track Recorder");
				File project_directory = new File(main_directory, project_name);
				File first_track = new File(project_directory, "first_track");
				FileInputStream fis_load = new FileInputStream(first_track);
				BufferedReader br = new BufferedReader(new InputStreamReader(fis_load));
				t1e = Boolean.parseBoolean(br.readLine());
				t1e = Boolean.parseBoolean(br.readLine());
				t2e = Boolean.parseBoolean(br.readLine());
				t3e = Boolean.parseBoolean(br.readLine());
				t4e = Boolean.parseBoolean(br.readLine());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//called when the menu button is pressed
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu){
	    	super.onCreateOptionsMenu(menu);
	    	MenuInflater inflater = getMenuInflater();
	    	inflater.inflate(R.menu.menu, menu);
	    	return true;
	    }
	    
	    //called when a menu item is selected
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item){
	    	switch(item.getItemId()){
	    	//if settings button is pressed launch instructions activity
	    	//can add more options to menu if needed
	    	case R.id.instructions:
	    		startActivity(new Intent(this, Instructions.class));
	    		return true;
	    	}
	    	return false;
	    }
}