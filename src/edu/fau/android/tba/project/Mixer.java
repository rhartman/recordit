package edu.fau.android.tba.project;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Mixer {
	public Track track1;
	public Track track2;
	public Track track3;
	public Track track4;
	private short [] mix_buffer;
	private byte [] mix_output;
	private short [] short_track_buffer;
	private int read = -1;
	private String project_name;
	
	//constructor
	Mixer(String name){
		project_name = new String(name);
		track1 = new Track("track1", project_name);
		track2 = new Track("track2", project_name);
		track3 = new Track("track3", project_name);
		track4 = new Track("track4", project_name);
		mix_buffer = new short [Track.buffer_size / 2];
		mix_output = new byte [Track.buffer_size];
		short_track_buffer = new short[Track.buffer_size / 2];
	}
	//outputs a mixed buffer of bytes
	public byte [] Output(){
		ClearMixBuffer();
		//only mix to buffer if track is not being recorded onto
		if(!track1.recording){
			Mix(track1, short_track_buffer);
		}
		if(!track2.recording){
			Mix(track2, short_track_buffer);
		}
		if(!track3.recording){
			Mix(track3, short_track_buffer);
		}
		if(!track4.recording){
			Mix(track4, short_track_buffer);
		}
		ShortsToBytes(mix_buffer, mix_output);
		return mix_output;
	}
	//mixes a track's buffer into the mix buffer
	private void Mix(Track track, short [] short_buffer){
		//if track exists, read from file into buffer
		if(track.exists && !track.recording){
			read = track.LoadBuffer();
			//if read is successful, convert buffer of bytes to shorts and add to the mix buffer
			if(read != -1){
				BytesToShorts(track.buffer, short_buffer);
				AddToMixBuffer(short_buffer, track.level);
			}
		}
	}
	//converts an array of bytes to an array of shorts
	//first parameter is array of bytes, second parameter is array of shorts
	private void BytesToShorts(byte [] byte_array,short [] short_array){
		ByteBuffer bb = ByteBuffer.allocate(byte_array.length);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		int j = 0;
		for(int i = 0; i < byte_array.length; i++){
			bb.put(byte_array [i]);
			i++;
			bb.put(byte_array [i]);
			short_array [j] = bb.getShort(i-1);
			j++;
		}
	}
	//converts an array of shorts to an array of bytes
	private void ShortsToBytes(short [] short_array, byte [] byte_array){
		ByteBuffer bb = ByteBuffer.allocate(byte_array.length);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		int j = 0;
		for(int i = 0; i< short_array.length; i++){
			bb.putShort(short_array[i]);
			byte_array[j] = bb.get(j);
			j++;
			byte_array[j] = bb.get(j );
			j++;
		}
	}
	//adds a track buffer to the mix buffer, scaled according to the level
	private void AddToMixBuffer(short [] track_buffer, int level){
		short temp;
		int mix_amplitude;
		for(int i = 0; i < track_buffer.length; i++){
			//scale down value by 2
			temp = (short) (track_buffer [i] / 2);
			//adjust level
			float vol = (float) level / 100;
			temp = (short) (temp * vol);
			//check amplitude of mix, to ensure no overflow
			mix_amplitude = mix_buffer [i] + temp;
			//if amplitude is greater than the max value for a short, default to max value
			//else put value into mix buffer
			if(mix_amplitude > 32767){
				mix_buffer [i] = 32767;
			}else{
				mix_buffer [i] = (short) mix_amplitude;
			}
		}
	}
	//clears the mix buffer of any data from previous transfer
	private void ClearMixBuffer(){
		for(int i = 0; i < mix_buffer.length; i++){
			mix_buffer[i] = 0;
		}
	}
}
