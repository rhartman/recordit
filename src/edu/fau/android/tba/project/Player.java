package edu.fau.android.tba.project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.System;
import java.io.FileInputStream;

import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.content.Context;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;

public class Player {
	public Mixer mixer;
	public boolean playing = false, recording = false, first = true;
	private AudioTrack output;
	private AudioRecord recorder;
	private Thread playThread, recThread, threadThread;
	private byte [] rec_buffer, orig_temp, new_temp;
	private long startTime, endTime, delayLength = 0;
	private double delaySize = 0;
	private FileInputStream is_temp = null;
	private String project_name;
	private File main_directory, project_directory, first_track;
	private FileOutputStream fos_save;
	
	Player(String name){
		project_name = new String(name);
		mixer = new Mixer(project_name);
		main_directory = new File(Environment.getExternalStorageDirectory(), "Multi Track Recorder");
		project_directory = new File(main_directory, project_name);
		Load();
		GetOffset();
	}
	
	//start recording
	public void onRecord(){
		recording = true;
		//record onto selected track
		if(mixer.track1.selected){
			Record(mixer.track1);
			recording = true;
		}else if(mixer.track2.selected){
			Record(mixer.track2);
			recording = true;
		}else if(mixer.track3.selected){
			Record(mixer.track3);
			recording = true;
		}else if(mixer.track4.selected){
			Record(mixer.track4);
			recording = true;
		}else{
			//cannot start recording - no track is selected
		}
	}
	private void Record(final Track track){
		track.recording = true;
		track.exists = true;
		Save();
		OpenTracks();
		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, Track.buffer_size);
		output = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_OUT_DEFAULT, AudioFormat.ENCODING_PCM_16BIT, Track.buffer_size, AudioTrack.MODE_STREAM);
		output.play();
		recorder.startRecording();
		recThread = new Thread(new Runnable() {
            @Override
            public void run() {
                    writeToFile(track);
                    }
            },"AudioRecord Thread");
    //start running thread
    recThread.start();
    Log.d("Record_Player", "Recording thread started.");
	}
	
	public void writeToFile(Track track){
		rec_buffer = new byte [Track.buffer_size];
		int read = 0;
		while(recording){
			if(recorder != null){
				read = recorder.read(rec_buffer, 0, Track.buffer_size);
				if(read != AudioRecord.ERROR_INVALID_OPERATION){
					try {
						output.write(mixer.Output(), 0, Track.buffer_size);
						if(track.fos != null){
							track.fos.write(rec_buffer);
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		}
		
		CloseTracks();
	}
	//start playing
	public void Play(){
		OpenTracks();
		playing = true;
		output = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_OUT_DEFAULT, AudioFormat.ENCODING_PCM_16BIT, Track.buffer_size, AudioTrack.MODE_STREAM);
		output.play();
		playThread = new Thread(new Runnable() {
            @Override
            public void run() {
                    writeToHardware();
                    }
            },"AudioTrack Thread");
    //start running thread
    playThread.start();
    Log.d("Play_Player", "Play thread has been started.");
	}
	//stop playing or recording
	public void Stop(){
		if(playing){
			playing = false;
			if(output != null){
				output.stop();
			}
		}else if(recording){
			//stop recording and release recorder
        	if(recorder != null){
        		recorder.stop();
                recorder.release();
                recorder = null;
        	}
			if(output != null){
				output.stop();
			}
			recording = false;
			if(mixer.track1.recording) {
				mixer.track1.recording = false;
				LatencyCorrection(mixer.track1);
			}else if(mixer.track2.recording) {
				mixer.track2.recording = false;
				LatencyCorrection(mixer.track2);
			}else if(mixer.track3.recording) {
				mixer.track3.recording = false;
				LatencyCorrection(mixer.track3);
			}else if(mixer.track4.recording) {
				mixer.track4.recording = false;
				LatencyCorrection(mixer.track4);
			}
			
		}
		Log.d("Stop_Player", "Audio playback stopped.");
	}
	//open track files
	private void OpenTracks(){
		if(mixer.track1.exists){
			if(mixer.track1.recording){
				mixer.track1.CreateOutputStream();
				Log.d("OpenTracks,", "Track 1 opened.");
			}else{
				mixer.track1.OpenFile();
			}
		}
		if(mixer.track2.exists){
			if(mixer.track2.recording){
				mixer.track2.CreateOutputStream();
			}else{
				mixer.track2.OpenFile();
			}
		}
		if(mixer.track3.exists){
			if(mixer.track3.recording){
				mixer.track3.CreateOutputStream();
			}else{
				mixer.track3.OpenFile();
			}
		}
		if(mixer.track4.exists){
			if(mixer.track4.recording){
				mixer.track4.CreateOutputStream();
			}else{
				mixer.track4.OpenFile();
			}
		}
		Log.d("OpenTracks,", "Tracks opened.");
	}
	//close track files
	private void CloseTracks(){
		if(mixer.track1.exists){
			mixer.track1.CloseFile();
		}
		if(mixer.track2.exists){
			mixer.track2.CloseFile();
		}
		if(mixer.track3.exists){
			mixer.track3.CloseFile();
		}
		if(mixer.track4.exists){
			mixer.track4.CloseFile();
		}
		Log.d("CloseTracks", "Tracks have been closed.");
	}
	//output data to audio hardware
	public void writeToHardware(){
		while(playing){
			output.write(mixer.Output(), 0, Track.buffer_size);
		}
		CloseTracks();
		Log.d("writeToHardware_Player", "Audio written to speaker for playback.");
	}
	
	//Offset newly recorded tracks by the audio hardware latency from microphone to speaker
	private void LatencyCorrection(Track track) {
		if(!first) {	
			int fsize = 0;
			try {
			try {
				is_temp = new FileInputStream(track.path);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
			fsize = is_temp.available();
				orig_temp = new byte [fsize];
				is_temp.read(orig_temp);
				//delayLength = 32000;
				new_temp = new byte [fsize];
			for(int i = 0; (i < new_temp.length - (int)delayLength); i++) {
				new_temp [i] = orig_temp[(int)delayLength + i];
			}
			for(int i=new_temp.length - (int)delayLength; i < new_temp.length; i++){
				new_temp[i] = 0;
			}
			if(track.fos != null){
				track.fos.close();
			}
				track.fos = new FileOutputStream(track.path, false);
				track.fos.write(new_temp);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.d("WriteToFile_Player", "Offset audio by hardware latency of 50274 bytes.");
		}
		first = false;
		Save();
		
	}
	private void Save() {
		first_track = new File(project_directory, "first_track");
			try {
				fos_save = new FileOutputStream(first_track);
				fos_save.write(Boolean.toString(first).getBytes());
				fos_save.write("\n".getBytes());
				fos_save.write(Boolean.toString(mixer.track1.exists).getBytes());
				fos_save.write("\n".getBytes());
				fos_save.write(Boolean.toString(mixer.track2.exists).getBytes());
				fos_save.write("\n".getBytes());
				fos_save.write(Boolean.toString(mixer.track3.exists).getBytes());
				fos_save.write("\n".getBytes());
				fos_save.write(Boolean.toString(mixer.track4.exists).getBytes());
				fos_save.write("\n".getBytes());
				fos_save.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	private void Load() {
		first_track = new File(project_directory, "first_track");
		if(!first_track.exists()) {
			try {
				fos_save = new FileOutputStream(first_track);
				fos_save.write("true".getBytes());
				fos_save.write("\n".getBytes());
				fos_save.write(Boolean.toString(mixer.track1.exists).getBytes());
				fos_save.write("\n".getBytes());
				fos_save.write(Boolean.toString(mixer.track2.exists).getBytes());
				fos_save.write("\n".getBytes());
				fos_save.write(Boolean.toString(mixer.track3.exists).getBytes());
				fos_save.write("\n".getBytes());
				fos_save.write(Boolean.toString(mixer.track4.exists).getBytes());
				fos_save.write("\n".getBytes());
				fos_save.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileInputStream fis;
		try {
			fis = new FileInputStream(first_track);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			first = Boolean.parseBoolean(br.readLine());
			mixer.track1.exists = Boolean.parseBoolean(br.readLine());
			mixer.track2.exists = Boolean.parseBoolean(br.readLine());
			mixer.track3.exists = Boolean.parseBoolean(br.readLine());
			mixer.track4.exists = Boolean.parseBoolean(br.readLine());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void GetOffset() {
		File m_settings = new File(project_directory, "m_settings");
		FileInputStream fis;
		String temporary = new String("");
		try {
			fis = new FileInputStream(m_settings);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			temporary = br.readLine();
			temporary = br.readLine();
			temporary = br.readLine();
			delayLength = Long.parseLong(br.readLine());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
