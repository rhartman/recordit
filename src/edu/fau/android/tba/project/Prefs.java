package edu.fau.android.tba.project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Prefs extends PreferenceActivity {
	private String name;
	private File main_directory, list, project_directory;
	private SharedPreferences prefs;
	private boolean click_on;
	private boolean first;
	private int tempo, loop_length, offset;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		setContentView(R.xml.settings);
	}
	
	public void CreateNewProject(View v){
		RetrieveSettings();
		CreateDirectories();
		list = new File(main_directory.getAbsolutePath(),"projectlist");
		try {
			//write project name to project list file
			FileOutputStream fos = new FileOutputStream(list.getAbsolutePath(), true);
			fos.write(name.getBytes());
			fos.write("\n".getBytes());
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Save(project_directory);
		Intent start_project = new Intent(Prefs.this, CEN4214_Project_TBAActivity.class);
		start_project.putExtra("name", name);
		//pass the project directory into project activity
		//start_project.putExtra("proj_dir", project_directory);
		startActivity(start_project);
	}
	
	private void CreateDirectories(){
		main_directory = new File(Environment.getExternalStorageDirectory(), "Multi Track Recorder");
		main_directory.mkdir();
		project_directory = new File(main_directory.getAbsolutePath(),name);
		project_directory.mkdir();
		//name += "\n";
	}
	
	private void RetrieveSettings(){
		//retrieve project name from text field
		name = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("project_name", "name");
		click_on = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean("click", true);
		String temp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("tempo", "120");
		tempo = Integer.parseInt(temp);
		temp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("loopLength", "32");
		loop_length = Integer.parseInt(temp);
		temp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("offset", "32000");
		offset = Integer.parseInt(temp);
	}
	
	private void Save(File project_directory){
		//Settings created here are saved in their own file so data from Project activity isn't overwritten
		File project_settings = new File(project_directory.getAbsolutePath(), "m_settings");
		String [] to_write = new String [3];
		to_write[0] = Boolean.toString(click_on);
		to_write[1] = Integer.toString(tempo);
		to_write[2] = Integer.toString(loop_length);
		FileOutputStream fos;
		try {
			//if file exists, it is truncated and new data is written.
			fos = new FileOutputStream(project_settings.getAbsolutePath());
			try {
				fos.write(to_write[0].getBytes());
				fos.write("\n".getBytes());
				fos.write(to_write[1].getBytes());
				fos.write("\n".getBytes());
				fos.write(to_write[2].getBytes());
				fos.write("\n".getBytes());
				fos.write(Integer.toString(offset).getBytes());
				fos.write("\n".getBytes());
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
