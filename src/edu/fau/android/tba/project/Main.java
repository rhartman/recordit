package edu.fau.android.tba.project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Main extends ListActivity {
	private String [] projects;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		InitializeArray();
		setListAdapter(new AwesomeAdapter());
		Toast toast = Toast.makeText(Main.this, "Click on the menu button for instructions", Toast.LENGTH_LONG);
        //toast.setMargin((float)0.5,(float)0.5);
        toast.show();
	}
	
	private void InitializeArray() {
		projects = new String [1];
		//This could also contain something like "There are no existing projects"
		projects[0] = "";
		File main_directory = new File(Environment.getExternalStorageDirectory(), "Multi Track Recorder");
		File list = new File(main_directory.getAbsolutePath(),"projectlist");
		FileInputStream fin = null;
		String temp;
		try {
			if(list.exists()){
				fin = new FileInputStream(list.getAbsolutePath());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if(fin != null){
			InputStreamReader isr = new InputStreamReader(fin);
			BufferedReader br = new BufferedReader(isr);
			try {
				br.mark(200);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			int i = 0;
			try {
				for(i = 0; br.readLine() != null; i++);
				br.reset();
				projects = new String [i];
				projects[0] = "";
				i=0;
				for(i = 0; (temp = br.readLine()) != null; i++)
				{
					projects[i] = temp;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				br.close();
				isr.close();
				fin.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			projects = new String [1];
			//This could also contain something like "There are no existing projects"
			projects[0] = "";
		}
	}
	//when a project name is clicked on, launch project activity with project name
	public void onListItemClick(ListView parent, View v,int position, long id) {
		Intent start_project = new Intent(Main.this, CEN4214_Project_TBAActivity.class);
		start_project.putExtra("name", projects [position]);
		startActivity(start_project);
		}
	
	public void NewProject(View v){
		Intent start_settings = new Intent(Main.this, Prefs.class);
		startActivity(start_settings);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		InitializeArray();
		setListAdapter(new AwesomeAdapter());
	}
	
	class AwesomeAdapter extends ArrayAdapter<String> {
		AwesomeAdapter() {
			super(Main.this, R.layout.row, R.id.label, projects);
			}
		public View getView(int position, View convertView,
				ViewGroup parent) {
			View row=super.getView(position, convertView, parent);
			return(row);
			}
		}
	//called when the menu button is pressed
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
    	super.onCreateOptionsMenu(menu);
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu, menu);
    	return true;
    }
    
    //called when a menu item is selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	//if settings button is pressed launch instructions activity
    	//can add more options to menu if needed
    	case R.id.instructions:
    		startActivity(new Intent(this, Instructions.class));
    		return true;
    	}
    	return false;
    }
	}