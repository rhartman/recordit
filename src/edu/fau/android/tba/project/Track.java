package edu.fau.android.tba.project;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.media.AudioFormat;
import android.media.AudioTrack;
import android.os.Environment;

public class Track {
	public boolean selected = false;
	public boolean exists = false;
	public boolean recording = false;
	static public int buffer_size = AudioTrack.getMinBufferSize(44100,AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT) * 2;
	private String filename;
	private File audioFile;
	public String path;
	public FileInputStream fis = null;
	private BufferedInputStream bis = null;
	public File project_directory;
	public FileOutputStream fos = null;
	public byte [] buffer;
	public int level = 50;
	
	//constructor
	Track(String name, String project){
    	//assign file name
		this.filename = new String(name);
		//calculate buffer size
    	buffer = new byte [buffer_size];
    	File main_directory = new File(Environment.getExternalStorageDirectory(), "Multi Track Recorder");
		project_directory = new File(main_directory.getAbsolutePath(),project);
    	//create file along with input file stream
    	this.CreateFile();
    	}
	//creates a new file, and input file stream
	public void CreateFile(){
		//create file instance
		audioFile = new File(project_directory.getAbsolutePath(), filename);
		//get file path
		path = new String(audioFile.getAbsolutePath());
	}
	//create output stream for recording
	public void CreateOutputStream(){
		try {
			fos = new FileOutputStream(path);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//opens file and input stream
	public void OpenFile(){
		try{
			fis = new FileInputStream(path);
    		bis = new BufferedInputStream(fis, buffer_size);
    	}catch(FileNotFoundException e){
    		e.printStackTrace();
    		}
	}
	//closes file
	public void CloseFile(){
		if(bis != null){
			try {
				bis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bis = null;
		}
		if(fis != null){
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			fis = null;
		}
		if(fos != null){
			try {
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			fos = null;
		}
	}
	//loads audio data from file into buffer
	public int LoadBuffer(){
		int read = 0;
		if(exists && !recording){
			try {
				if(bis != null){
					read = bis.read(buffer, 0 , buffer_size);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				read = -1;
			}
		}
		return read;
	}
}
