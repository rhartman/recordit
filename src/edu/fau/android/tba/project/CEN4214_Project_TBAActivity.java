package edu.fau.android.tba.project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import android.view.OrientationEventListener;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

public class CEN4214_Project_TBAActivity extends Activity implements SeekBar.OnSeekBarChangeListener,OnLongClickListener{
	private Player player;
	private String project_name;
	private String track_selected = new String("track1");
	private File main_directory, project_directory, audio_settings;
	private Drawable btn;
	
	//track select checkboxes
	private CheckBox selectTrack1, selectTrack2, selectTrack3, selectTrack4;
	
	//track level control seekbars
	private SeekBar levelControlTrack1, levelControlTrack2, levelControlTrack3, levelControlTrack4;
	
	//transport buttons
	private Button record, play, stop;
	
	private TextView heading, nametrack1,nametrack2,nametrack3,nametrack4;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.project);
        Bundle extras = getIntent().getExtras(); 
        project_name = new String(extras.getString("name"));
        main_directory = new File(Environment.getExternalStorageDirectory(), "Multi Track Recorder");
        project_directory = new File(main_directory, project_name);
        heading = (TextView)findViewById(R.id.heading);
        heading.setText(project_name);

        
      //set up listeners for level controls
        levelControlTrack1 = (SeekBar)findViewById(R.id.levelControlTrack1);
        levelControlTrack1.setOnSeekBarChangeListener(this);
        levelControlTrack2 = (SeekBar)findViewById(R.id.levelControlTrack2);
        levelControlTrack2.setOnSeekBarChangeListener(this);
        levelControlTrack3 = (SeekBar)findViewById(R.id.levelControlTrack3);
        levelControlTrack3.setOnSeekBarChangeListener(this);
        levelControlTrack4 = (SeekBar)findViewById(R.id.levelControlTrack4);
        levelControlTrack4.setOnSeekBarChangeListener(this);
        
        //find checkboxes by id
        selectTrack1 = (CheckBox)findViewById(R.id.selectTrack1);
        selectTrack1.setChecked(true);
        selectTrack2 = (CheckBox)findViewById(R.id.selectTrack2);
        selectTrack3 = (CheckBox)findViewById(R.id.selectTrack3);
        selectTrack4 = (CheckBox)findViewById(R.id.selectTrack4);
        //instantiate player
        player = new Player(project_name);
        
        //find buttons by id
        play = (Button)findViewById(R.id.play);
        record = (Button)findViewById(R.id.record);
        stop = (Button)findViewById(R.id.stop);
        Load();
        player.mixer.track1.selected = true;
        player.mixer.track2.selected = false;
        player.mixer.track3.selected = false;
        player.mixer.track4.selected = false;
        
        nametrack1 = (TextView)findViewById(R.id.nameTrack1);
        nametrack1.setOnLongClickListener(this);
        nametrack2 = (TextView)findViewById(R.id.nameTrack2);
        nametrack2.setOnLongClickListener(this);
        nametrack3 = (TextView)findViewById(R.id.nameTrack3);
        nametrack3.setOnLongClickListener(this);
        nametrack4 = (TextView)findViewById(R.id.nameTrack4);
        nametrack4.setOnLongClickListener(this);
    }
    
    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Save();
        Intent start_trackview = new Intent(CEN4214_Project_TBAActivity.this, TrackView.class);
        start_trackview.putExtra("track_selected", track_selected);
        start_trackview.putExtra("name", project_name);
        startActivity(start_trackview);
    }*/

    //called when the menu button is pressed
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
    	super.onCreateOptionsMenu(menu);
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.menu, menu);
    	return true;
    }
    
    //called when a menu item is selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	//if settings button is pressed launch instructions activity
    	//can add more options to menu if needed
    	case R.id.instructions:
    		startActivity(new Intent(this, Instructions.class));
    		return true;
    	}
    	return false;
    }
    
    //called when a track select checkbox is checked
    public void onTrackSelect(View v){
    	switch(v.getId()){
    	case R.id.selectTrack1:
    		//select track 1 and unselect all other tracks
    		if(!player.mixer.track1.selected){
    			player.mixer.track1.selected = true;
        		player.mixer.track2.selected = false;
        		selectTrack2.setChecked(false);
        		player.mixer.track3.selected = false;
        		selectTrack3.setChecked(false);
        		player.mixer.track4.selected = false;
        		selectTrack4.setChecked(false);
    		}else{
    			selectTrack1.setChecked(true);
    		}
    		track_selected = new String("track1");
    		break;
    	case R.id.selectTrack2:
    		//select track 2 and unselect all other tracks
    		if(!player.mixer.track2.selected){
    			player.mixer.track2.selected = true;
        		player.mixer.track1.selected = false;
        		selectTrack1.setChecked(false);
        		player.mixer.track3.selected = false;
        		selectTrack3.setChecked(false);
        		player.mixer.track4.selected = false;
        		selectTrack4.setChecked(false);
    		}else{
    			selectTrack2.setChecked(true);
    		}
    		track_selected = new String("track2");
    		break;
    	case R.id.selectTrack3:
    		//select track 3 and unselect all other tracks
    		if(!player.mixer.track3.selected){
    			player.mixer.track3.selected = true;
        		player.mixer.track2.selected = false;
        		selectTrack2.setChecked(false);
        		player.mixer.track1.selected = false;
        		selectTrack1.setChecked(false);
        		player.mixer.track4.selected = false;
        		selectTrack4.setChecked(false);
    		}else{
    			selectTrack3.setChecked(true);
    		}
    		track_selected = new String("track3");
    		break;
    	case R.id.selectTrack4:
    		//select track 4 and unselect all other tracks
    		if(!player.mixer.track4.selected){
    			player.mixer.track4.selected = true;
        		player.mixer.track2.selected = false;
        		selectTrack2.setChecked(false);
        		player.mixer.track3.selected = false;
        		selectTrack3.setChecked(false);
        		player.mixer.track1.selected = false;
        		selectTrack1.setChecked(false);
    		}else{
    			selectTrack4.setChecked(true);
    		}
    		track_selected = new String("track4");
    		break;
    	}
    }
    
    //called when a transport button is pressed
    public void onButtonClick(View v){
    	switch(v.getId()){
    	case R.id.record:
    		//start recording
    		player.onRecord();
    		btn = record.getBackground();
    		record.setBackgroundColor(Color.RED);
    		break;
    	case R.id.play:
    		if(!player.playing){
    			//start playing
        		player.Play();
        		btn = play.getBackground();
        		play.setBackgroundColor(Color.GREEN);
    		}
    		break;
    	case R.id.stop:
    		if(player.playing){
    			//stop playing or recording
        		player.Stop();
        		play.setBackgroundDrawable(btn);
    		}
    		if(player.recording){
    			player.Stop();
    			record.setBackgroundDrawable(btn);
    		}
    		break;
    	}
    }
    
    //called when a track's level is adjusted
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		switch(seekBar.getId()){
		case R.id.levelControlTrack1:
			//adjust mix level of track 1
			player.mixer.track1.level = progress;
			break;
		case R.id.levelControlTrack2:
			//adjust mix level of track 2
			player.mixer.track2.level = progress;
			break;
		case R.id.levelControlTrack3:
			//adjust mix level of track 3
			player.mixer.track3.level = progress;
			break;
		case R.id.levelControlTrack4:
			//adjust mix level of track 4
			player.mixer.track4.level = progress;
			break;
		}
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		//not implemented
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		//not implemented
	}
	
	private void Save(){
		audio_settings = new File(project_directory.getAbsolutePath(), "a_settings");
		try {
			FileOutputStream fos = new FileOutputStream(audio_settings);
			try {
				fos.write(Boolean.toString(player.mixer.track1.selected).getBytes());
				fos.write("\n".getBytes());
				fos.write(Boolean.toString(player.mixer.track2.selected).getBytes());
				fos.write("\n".getBytes());
				fos.write(Boolean.toString(player.mixer.track3.selected).getBytes());
				fos.write("\n".getBytes());
				fos.write(Boolean.toString(player.mixer.track4.selected).getBytes());
				fos.write("\n".getBytes());
				fos.write(Boolean.toString(player.mixer.track1.exists).getBytes());
				fos.write("\n".getBytes());
				fos.write(Boolean.toString(player.mixer.track2.exists).getBytes());
				fos.write("\n".getBytes());
				fos.write(Boolean.toString(player.mixer.track3.exists).getBytes());
				fos.write("\n".getBytes());
				fos.write(Boolean.toString(player.mixer.track4.exists).getBytes());
				fos.write("\n".getBytes());
				fos.write(Integer.toString(player.mixer.track1.level).getBytes());
				fos.write("\n".getBytes());
				fos.write(Integer.toString(player.mixer.track2.level).getBytes());
				fos.write("\n".getBytes());
				fos.write(Integer.toString(player.mixer.track3.level).getBytes());
				fos.write("\n".getBytes());
				fos.write(Integer.toString(player.mixer.track4.level).getBytes());
				fos.write("\n".getBytes());
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*SharedPreferences prefs = getSharedPreferences(project_name, MODE_PRIVATE);
		SharedPreferences.Editor ed = prefs.edit();
		ed.putBoolean("track1_selected", player.mixer.track1.selected);
		ed.putBoolean("track2_selected", player.mixer.track2.selected);
		ed.putBoolean("track3_selected", player.mixer.track3.selected);
		ed.putBoolean("track4_selected", player.mixer.track4.selected);
		ed.putBoolean("track1_exists", player.mixer.track1.exists);
		ed.putBoolean("track2_exists", player.mixer.track2.exists);
		ed.putBoolean("track3_exists", player.mixer.track3.exists);
		ed.putBoolean("track4_exists", player.mixer.track4.exists);
		ed.putInt("track1_level", player.mixer.track1.level);
		ed.putInt("track2_level", player.mixer.track2.level);
		ed.putInt("track3_level", player.mixer.track3.level);
		ed.putInt("track4_level", player.mixer.track4.level);
		ed.commit();*/
		Log.d("Save_Project", "Shared preferences saved.");
	}
	
	private void Load(){
		audio_settings = new File(project_directory.getAbsolutePath(), "a_settings");
		if(!audio_settings.exists()) {
			player.mixer.track1.selected = false;
			player.mixer.track2.selected = false;
			player.mixer.track3.selected = false;
			player.mixer.track4.selected = false;
			player.mixer.track1.exists = false;
			player.mixer.track2.exists = false;
			player.mixer.track3.exists = false;
			player.mixer.track4.exists = false;
			player.mixer.track1.level = 50;
			levelControlTrack1.setProgress(player.mixer.track1.level);
			player.mixer.track2.level = 50;
			levelControlTrack2.setProgress(player.mixer.track2.level);
			player.mixer.track3.level = 50;
			levelControlTrack3.setProgress(player.mixer.track3.level);
			player.mixer.track4.level = 50;
			levelControlTrack4.setProgress(player.mixer.track4.level);
			return;
		}
		try {
			FileInputStream fis = new FileInputStream(audio_settings);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			try {
				player.mixer.track1.selected = Boolean.parseBoolean(br.readLine());
				player.mixer.track2.selected = Boolean.parseBoolean(br.readLine());
				player.mixer.track3.selected = Boolean.parseBoolean(br.readLine());
				player.mixer.track4.selected = Boolean.parseBoolean(br.readLine());
				player.mixer.track1.exists = Boolean.parseBoolean(br.readLine());
				player.mixer.track2.exists = Boolean.parseBoolean(br.readLine());
				player.mixer.track3.exists = Boolean.parseBoolean(br.readLine());
				player.mixer.track4.exists = Boolean.parseBoolean(br.readLine());
				player.mixer.track1.level = Integer.parseInt(br.readLine());
				levelControlTrack1.setProgress(player.mixer.track1.level);
				player.mixer.track2.level = Integer.parseInt(br.readLine());
				levelControlTrack2.setProgress(player.mixer.track2.level);
				player.mixer.track3.level = Integer.parseInt(br.readLine());
				levelControlTrack3.setProgress(player.mixer.track3.level);
				player.mixer.track4.level = Integer.parseInt(br.readLine());
				levelControlTrack4.setProgress(player.mixer.track4.level);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*SharedPreferences prefs = getSharedPreferences(project_name, MODE_PRIVATE);
		player.mixer.track1.selected = prefs.getBoolean("track1_selected", false);
		player.mixer.track2.selected = prefs.getBoolean("track2_selected", false);
		player.mixer.track3.selected = prefs.getBoolean("track3_selected", false);
		player.mixer.track4.selected = prefs.getBoolean("track4_selected", false);
		player.mixer.track1.exists = prefs.getBoolean("track1_exists", false);
		player.mixer.track2.exists = prefs.getBoolean("track2_exists", false);
		player.mixer.track3.exists = prefs.getBoolean("track3_exists", false);
		player.mixer.track4.exists = prefs.getBoolean("track4_exists", false);
		player.mixer.track1.level = prefs.getInt("track1_level", 50);
		levelControlTrack1.setProgress(player.mixer.track1.level);
		player.mixer.track2.level = prefs.getInt("track2_level", 50);
		levelControlTrack2.setProgress(player.mixer.track2.level);
		player.mixer.track3.level = prefs.getInt("track3_level", 50);
		levelControlTrack3.setProgress(player.mixer.track3.level);
		player.mixer.track4.level = prefs.getInt("track4_level", 50);
		levelControlTrack4.setProgress(player.mixer.track4.level);
		Log.d("Load_Project", "Shared preferences loaded.");*/
	}
	
	@Override
	public void onPause(){
		super.onPause();
		Save();
	}
	
	@Override
	public void onStop(){
		super.onStop();
		Save();
	}

	@Override
	public boolean onLongClick(View arg0) {
		// TODO Auto-generated method stub
		Save();
        Intent start_trackview = new Intent(CEN4214_Project_TBAActivity.this, TrackView.class);
        start_trackview.putExtra("name", project_name);
		switch(arg0.getId()){
		case R.id.nameTrack1:
	        start_trackview.putExtra("track_selected", "track1");
	        break;
		case R.id.nameTrack2:
	        start_trackview.putExtra("track_selected", "track2");
	        break;
		case R.id.nameTrack3:
	        start_trackview.putExtra("track_selected", "track3");
	        break;
		case R.id.nameTrack4:
	        start_trackview.putExtra("track_selected", "track4");
	        break;
	        }
		startActivity(start_trackview);
		return false;
	}
}
