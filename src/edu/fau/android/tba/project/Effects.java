package edu.fau.android.tba.project;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.util.Log;

public class Effects {
	private byte [] trackBytes;
	private short [] trackShorts;
	
	//Reverses an array of shorts and converts back to bytes for output to file
	public void Reverse(Track track) {
		File newTrack;
		FileOutputStream newOutput;
		byte [] bytesTemp;
		short [] shortsTempForward, shortsTempReverse;
		createBackupTrack(track);
		track.OpenFile();
		try {
			bytesTemp = new byte [track.fis.available()];
			shortsTempForward = new short [(track.fis.available()) / 2];
			shortsTempReverse = new short [(track.fis.available()) / 2];
			track.fis.read(bytesTemp);
			BytesToShorts(bytesTemp, shortsTempForward);
			int j = 0;
			for(int i = (shortsTempForward.length) - 1; i > 0; i--) {
				shortsTempReverse[j++] = shortsTempForward[i];
			}
			ShortsToBytes(shortsTempReverse, bytesTemp);
			newTrack = new File(track.path);
			newOutput = new FileOutputStream(newTrack);
			newOutput.write(bytesTemp);
			Log.d("Reverse_Effects", "Audio data reversed and saved.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Converts an array of bytes into an array of shorts.
	//This is used to process 16-bit audio samples.
	private void BytesToShorts(byte [] byte_array,short [] short_array){
		ByteBuffer bb = ByteBuffer.allocate(byte_array.length);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		int j = 0;
		for(int i = 0; i < byte_array.length; i++){
			bb.put(byte_array [i]);
			i++;
			bb.put(byte_array [i]);
			short_array [j] = bb.getShort(i-1);
			j++;
		}
	}
	//converts an array of shorts to an array of bytes
	private void ShortsToBytes(short [] short_array, byte [] byte_array){
		ByteBuffer bb = ByteBuffer.allocate(byte_array.length);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		int j = 0;
		for(int i = 0; i< short_array.length; i++){
			bb.putShort(short_array[i]);
			byte_array[j] = bb.get(j);
			j++;
			byte_array[j] = bb.get(j );
			j++;
		}
	}
	//Writes a backup of the original audio file to trackname_backup
	//This occurs before an effect is applied
	private void createBackupTrack(Track track) {
		File backup;
		FileOutputStream backupout;
		String backup_path = new String(track.path);
		backup_path = backup_path + "_backup";
		track.OpenFile();
		try {
			trackBytes = new byte [track.fis.available()];
			track.fis.read(trackBytes);
			backup = new File(backup_path);
			backupout = new FileOutputStream(backup);
			backupout.write(trackBytes);
			backupout.close();
			track.CloseFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Creates a new audio track that contains the original plus 2 others offset by 250ms and 500ms each
	public void Delay(Track track) {
		File newTrack;
		FileOutputStream newOutput;
		byte [] bytesTemp;
		short [] shortsOrig, shortsDelay1, shortsDelay2, shortsOutput;
		createBackupTrack(track);
		track.OpenFile();
		try {
			bytesTemp = new byte [(track.fis.available()) + 44100];
			track.fis.read(bytesTemp, 0, track.fis.available());
			track.CloseFile();
			track.OpenFile();
			for(int i = track.fis.available(); i < bytesTemp.length; i++) {
				bytesTemp[i] = 0;
			}
			shortsOrig = new short [(bytesTemp.length) / 2];
			shortsDelay1 = new short [(bytesTemp.length) / 2];
			shortsDelay2 = new short [(bytesTemp.length) / 2];
			shortsOutput = new short [(bytesTemp.length) / 2];
			BytesToShorts(bytesTemp, shortsOrig);
			for(int i = 0; i < 11025; i++) {
				shortsDelay1[i] = 0;
			}
			int j = 0;
			for(int i = 11025; i < shortsOrig.length; i++) {
				shortsDelay1[i] = (short) (shortsOrig[j] / 4);
				j++;
			}
			for(int i = 0; i < 22050; i++) {
				shortsDelay2[i] = 0;
			}
			j = 0;
			for(int i = 22050; i < shortsOrig.length; i++) {
				shortsDelay2[i] = (short) (shortsOrig[j] / 8);
				j++;
			}
			for(int i = 0; i < shortsOutput.length; i++) {
				shortsOutput[i] = shortsOrig[i];
				shortsOutput[i] += shortsDelay1[i];
				shortsOutput[i] += shortsDelay2[i];
			}
			ShortsToBytes(shortsOutput, bytesTemp);
			newTrack = new File(track.path);
			newOutput = new FileOutputStream(newTrack);
			newOutput.write(bytesTemp);
			track.CloseFile();
			Log.d("Delay_Effects", "Delay track written successfully.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

